import React, {useState, useEffect} from 'react';
import Axios from 'axios';
import { Form, Button, Container } from 'react-bootstrap';
import Header from '../Pages/Header'

function SearchPlayerForm() {
    const [username,setUsername] = useState('')
    const [email,setEmail] = useState('')
    const [experience,setExperience] = useState(0)
    const [lvl,setLvl] = useState(0) 
    const onChangeUsername = (e) => {
        const data = e.target.value
        setUsername(data);
    }
    const onChangeEmail = (e) => {
        const data = e.target.value
        setEmail(data);
    }
    const onChangeExperience = (e) => {
        const data = e.target.value
        setExperience(data);
    }
    const onChangeLvl = (e) => {
        const data = e.target.value
        setLvl(data);
    }
    const onSubmitSearch = () => {
        const dataFilter = {
            username:username,
            email:email,
            exp:experience,
            lvl:lvl
        }
        console.log(dataFilter);
        Axios.get('http://localhost:5000/api/players',dataFilter)
    }

    return (
        <div>
             <Container>
                <Header></Header>
                <Form>
                    <Form.Group>
                        <Form.Label>User Name</Form.Label>
                        <Form.Control placeholder="username" type="text" name="username" value={username} onChange={onChangeUsername} />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Email</Form.Label>
                        <Form.Control placeholder="email" type="text" name="email" value={email} onChange={onChangeEmail} />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Experience</Form.Label>
                        <Form.Control placeholder="experience" type="number" name="experience" value={experience} onChange={onChangeExperience} />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Level</Form.Label>
                        <Form.Control placeholder="level" type="number" name="lvl" value={lvl} onChange={onChangeLvl} />
                    </Form.Group>

                    <Button variant="primary" type="submit" value="Submit" onClick={onSubmitSearch}> Search
                    </Button>
                </Form>
            </Container>
        </div>
    )
}

export default SearchPlayerForm

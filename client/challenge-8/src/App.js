import NewPlayerForm from './components/NewPlayerForm';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { Fragment } from 'react';
import Home from './components/Home';
import 'bootstrap/dist/css/bootstrap.min.css';
import SearchPlayerForm from './components/SearchPlayerForm';
import EditPlayerForm from './components/EditPlayerForm';

function App() {
  return (
    <Fragment>
      <Router>
        <Switch>
          <Route path ='/new'>
            <NewPlayerForm />
          </Route>
          <Route path ='/edit'>
            <EditPlayerForm/>
          </Route>
          <Route path ='/search'>
            <SearchPlayerForm />
          </Route>
          <Route path ='/home'>
            <Home />
          </Route>
        </Switch>
      </Router>
    </Fragment>
  
  );
}

export default App;

import React from 'react'
import { Nav } from 'react-bootstrap';

function Header() {
    return (
        <div>
            <Nav fill variant="tabs">
            <Nav.Item>
                <Nav.Link href="/home">Home</Nav.Link>
            </Nav.Item>
            <Nav.Item>
                <Nav.Link href="/new">New Player Form</Nav.Link>
            </Nav.Item>
            <Nav.Item>
                <Nav.Link href="/edit">Edit Player Form</Nav.Link>
            </Nav.Item>
            <Nav.Item>
                <Nav.Link href="/search">Search Player Form</Nav.Link>
            </Nav.Item>
            </Nav>
        </div>
    )
}

export default Header

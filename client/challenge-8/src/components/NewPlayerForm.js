import React, {useState} from 'react';
import Axios from 'axios';
import { Form, Button, Container } from 'react-bootstrap';
import Header from '../Pages/Header'


function NewPlayerForm() {
    const [username,setUsername] = useState('')
    const [email,setEmail] = useState('')
    const [password,setPassword] = useState('')
    const [experience,setExperience] = useState(0)
    const [lvl,setLvl] = useState(0) 
    const onChangeUsername = (e) => {
        const data = e.target.value
        setUsername(data);
    }
    const onChangeEmail = (e) => {
        const data = e.target.value
        setEmail(data);
    }
    const onChangePassword = (e) => {
        const data = e.target.value
        setPassword(data);
    }
    const onChangeExperience = (e) => {
        const data = e.target.value
        setExperience(data);
    }
    const onChangeLvl = (e) => {
        const data = e.target.value
        setLvl(data);
    }
    const onSubmitNewPlayer = () => {
        const data = {
            username:username,
            email:email,
            password:password,
            exp:experience,
            lvl:lvl
        }
        console.log(data);
        Axios.post('http://localhost:5000/api/players',data)
    }

    return (
        <div>
            <Container>
                <Header></Header>
                <Form>
                    <Form.Group>
                        <Form.Label>User Name</Form.Label>
                        <Form.Control placeholder="username" type="text" name="username" value={username} onChange={onChangeUsername} />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Email</Form.Label>
                        <Form.Control placeholder="email" type="text" name="email" value={email} onChange={onChangeEmail} />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control placeholder="password" type="text" name="password" value={password} onChange={onChangePassword} />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Experience</Form.Label>
                        <Form.Control placeholder="experience" type="number" name="experience" value={experience} onChange={onChangeExperience} />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Level</Form.Label>
                        <Form.Control placeholder="level" type="number" name="lvl" value={lvl} onChange={onChangeLvl} />
                    </Form.Group>

                    <Button variant="primary" type="submit" value="Submit" onClick={onSubmitNewPlayer}> Submit
                    </Button>
                </Form>
            </Container>
        </div>
    )
}

export default NewPlayerForm
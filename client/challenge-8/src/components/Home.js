import React from 'react'
import { Container } from 'react-bootstrap'
import Header from '../Pages/Header'

function Home() {
    return (
        <div>
          <Container>
            <Header> </Header>
            Hello World
          </Container>
        </div>
    )
}

export default Home
